#include <stdlib.h>
#include <stdio.h>

#define BFLT_HEADER_SIZE 0x40

int main(int argc, char **argv) {
	FILE *bflt_file = fopen(argv[1], "rb");
	FILE *tmp_file = fopen("/tmp/bflt_content.gz", "wb");
	FILE *new_file = fopen(argv[2], "wb");

	if(bflt_file == NULL) {
		printf("Error opening file or file does not exist.");
		return 1;
	}

	fseek(bflt_file, 0L, SEEK_END);
	unsigned long file_size = ftell(bflt_file);
	fseek(bflt_file, 0L, SEEK_SET);

	char *bflt_header = malloc(sizeof(char) * BFLT_HEADER_SIZE);
	char *bflt_content = malloc(sizeof(char) * file_size);

	fread(bflt_header, 1, BFLT_HEADER_SIZE, bflt_file);
	bflt_header[0x27] = bflt_header[0x27] & 3; 		//Change header flag from compressed to uncompressed

	fseek(bflt_file, 0x40L, SEEK_SET);
	fread(bflt_content, 1, file_size-0x40, bflt_file);
	fwrite(bflt_content, 1, file_size-0x40, tmp_file);
	fclose(tmp_file);
	system("gzip -d /tmp/bflt_content.gz");

	FILE *raw_file = fopen("/tmp/bflt_content", "rb");
	fseek(raw_file, 0L, SEEK_END);
	unsigned long raw_file_size = ftell(raw_file);
	fseek(raw_file, 0L, SEEK_SET);

	char *raw_content = malloc(sizeof(char) * raw_file_size);
	fread(raw_content, 1, raw_file_size, raw_file);

	fwrite(bflt_header, 1, 0x40, new_file);
	fseek(new_file, 0x40L, SEEK_SET);
	fwrite(raw_content, 1, raw_file_size, new_file);

	fclose(new_file);
	fclose(bflt_file);
	fclose(raw_file);

	free(raw_content);
	free(bflt_header);
	free(bflt_content);

	return 0;
}
